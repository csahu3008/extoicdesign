from django.urls import include,path
from .views import Addimage,RaiseRequest,RaiseRequestDetail,Approve,ApproveDetail,CustomerDataList,GalleryList,AdminApprovedList
urlpatterns =[
   path('image/',Addimage.as_view(),name='addimage'),
   path('request/',RaiseRequest.as_view(),name='raiserequest'),
   path('request_detail/<int:pk>',RaiseRequestDetail.as_view(),name='request_detail'),
    path('approve/',Approve.as_view(),name='approve'),
   path('approve_detail/<int:pk>',ApproveDetail.as_view(),name='approve_detail'),
   path('dashboard/',CustomerDataList.as_view(),name='dashboard'),
   path('listapproved/',AdminApprovedList.as_view(),name='adashboard'),
   path('gallery/',GalleryList.as_view(),name='gallery')
] 