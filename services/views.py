from django.shortcuts import reverse
from django.http import HttpResponse
from django.shortcuts import render
from django.core.exceptions import PermissionDenied
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin
from .models import Imagesection,Category,Raiserequest,Approverequest
from django.views.generic import CreateView,DetailView,ListView
from .forms import ApprovalForm


class Addimage(LoginRequiredMixin,CreateView):
    template_name='services/Addimage.html'
    success_url=reverse_lazy('home')
    model=Imagesection
    login_url='login'
    fields=['title','image','category']
    def form_valid(self, form): # new
            form.instance.owner = self.request.user
            return super().form_valid(form)
    def dispatch(self,request,*args,**kwargs):
        if self.request.user.is_admin:
            return super().dispatch(request,*args,**kwargs)
        else:
            raise PermissionDenied
class RaiseRequest(LoginRequiredMixin,CreateView):
    template_name='services/request.html'
    # success_url=reverse_lazy('home')
    model=Raiserequest
    login_url='login'
    fields=['title','short_description','description','related_docs']
    def form_valid(self, form): 
            form.instance.raised_by = self.request.user
            return super().form_valid(form)
    def dispatch(self,request,*args,**kwargs):
        if self.request.user.is_admin:
            raise PermissionDenied
        else:
            return super().dispatch(request,*args,**kwargs)
        
class RaiseRequestDetail(LoginRequiredMixin, DetailView): # new
    model = Raiserequest
    template_name = 'services/request_detail.html'
    login_url = 'login'
    def dispatch(self,request,*args,**kwargs):
        if self.request.user.is_admin:
            raise PermissionDenied
        else:
            return super().dispatch(request,*args,**kwargs)
class Approve(LoginRequiredMixin,CreateView):
    template_name='services/Approve.html'
    form_class=ApprovalForm
    login_url='login'
    def dispatch(self,request,*args,**kwargs):
        if self.request.user.is_admin:
            return super().dispatch(request,*args,**kwargs)
        else:
            raise PermissionDenied

class ApproveDetail(LoginRequiredMixin, DetailView): 
    model=Approverequest
    template_name = 'services/Approve_detail.html'
    login_url = 'login'
    def dispatch(self,request,*args,**kwargs):
        if self.request.user.is_admin:
            return super().dispatch(request,*args,**kwargs)
        else:
            raise PermissionDenied

class CustomerDataList(LoginRequiredMixin,ListView):
    model=Raiserequest
    login_url='login'
    template_name='services/CustomerList.htm'
    def get_queryset(self,*args,**kwargs):
        qs=super().get_queryset(*args,**kwargs) 
        return qs.filter(raised_by=self.request.user).order_by('status')
        # return qs
class AdminApprovedList(LoginRequiredMixin,ListView):
    model=Raiserequest
    login_url='login'
    template_name='services/CustomerList.htm'
    def get_queryset(self,*args,**kwargs):
        qs=super().get_queryset(*args,**kwargs) 
        return qs.filter().order_by('status')
class GalleryList(ListView):
    model=Imagesection
    login_url='login'
    template_name='services/Gallery.html'