from django.contrib import admin
from django.contrib.admin import StackedInline
from  .models import Imagesection,Category,Approverequest,Raiserequest
# Register your models here.
class ImageInline(admin.TabularInline):
    model=Imagesection
class CategoryAdmin(admin.ModelAdmin):
    inlines=[ImageInline,]
admin.site.register(Category,CategoryAdmin)
admin.site.register(Approverequest)
admin.site.register(Raiserequest)