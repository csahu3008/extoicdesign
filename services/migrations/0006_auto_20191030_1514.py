# Generated by Django 2.2.6 on 2019-10-30 15:14

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('services', '0005_auto_20191030_1454'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='approverequest',
            options={'verbose_name_plural': 'Approve Request'},
        ),
        migrations.AlterModelOptions(
            name='imagesection',
            options={'verbose_name_plural': 'Image Section'},
        ),
        migrations.AlterModelOptions(
            name='raiserequest',
            options={'verbose_name_plural': 'Raise Request'},
        ),
        migrations.AlterField(
            model_name='approverequest',
            name='issue',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='services.Raiserequest', verbose_name='Request'),
        ),
        migrations.AlterField(
            model_name='raiserequest',
            name='raised_by',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='Raised By'),
        ),
        migrations.AlterField(
            model_name='raiserequest',
            name='related_docs',
            field=models.ImageField(blank=True, upload_to='request/', verbose_name='Similar Image'),
        ),
    ]
