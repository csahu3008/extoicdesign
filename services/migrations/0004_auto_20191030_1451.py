# Generated by Django 2.2.6 on 2019-10-30 14:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('services', '0003_approverequest'),
    ]

    operations = [
        migrations.AlterField(
            model_name='raiserequest',
            name='status',
            field=models.CharField(blank=True, choices=[('0', 'pending'), ('1', 'approved'), ('2', 'rejected')], default='0', max_length=1),
        ),
    ]
