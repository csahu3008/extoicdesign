from django.conf import settings
from django.shortcuts import reverse
from django.db import models
from django.urls import reverse_lazy
from django.http import HttpResponse
from django.core.exceptions import PermissionDenied
from django.utils.translation import ugettext_lazy as _
# Create your models here.
class Category(models.Model):
    title=models.CharField(max_length=300)
    def __str__(self):
        return self.title
class Imagesection(models.Model):
    title=models.CharField(_('Title'),max_length=300)
    image=models.ImageField(upload_to='site_images/',max_length=1000)
    date_added=models.DateField(_('Date Added'),auto_now_add=True)
    category=models.ForeignKey(Category,on_delete=models.CASCADE)
    owner=models.ForeignKey(settings.AUTH_USER_MODEL,on_delete=models.CASCADE)
    class Meta:
        verbose_name_plural=_('Image Section')
    def __str__(self):
        return '{} was added on {}'.format(self.title,self.date_added)
    # def get_absolute_url(self):
    #     return reverse('images-section-detail',kwargs={'pk':self.pk})
    def save(self,*args,**kwargs):
        if(self.owner.is_admin):
            super(Imagesection,self).save()
        else:
            return PermissionDenied
status=((0,'pending'),(1,'approved'),(2,'rejected'))
class Raiserequest(models.Model):
    title=models.CharField(_('Title'),unique=True,max_length=200)
    short_description=models.CharField(_('Short Description'),max_length=400)
    description=models.TextField(_('Detail'))
    related_docs=models.ImageField(upload_to='request/',blank=True,verbose_name=_('Similar Image'))
    status=models.CharField(choices=status,blank=True,max_length=1,default=status[0][0])
    raised_by=models.ForeignKey(settings.AUTH_USER_MODEL,on_delete=models.CASCADE,verbose_name=_('Raised By'))
    date_updated=models.DateField(_('Last Updated'),auto_now=True)
    date_added=models.DateField(_('Date Added'),auto_now_add=True)
    def __str__(self):
        return self.title
    def get_absolute_url(self):
        return reverse('request_detail',args=[str(self.id)])
    @property
    def get_status(self):
        if self.status:
            return status[int(self.status)][1]
    class Meta:
        verbose_name_plural=_('Raise Request')

class Approverequest(models.Model):
    issue=models.OneToOneField(Raiserequest,on_delete=models.CASCADE,verbose_name=_('Request'))
    comment=models.CharField(_('Comment'),unique=True,max_length=500)
    document=models.FileField(upload_to='document/',blank=True)
    date_updated=models.DateField(_('Last Updated'),auto_now=True)
    date_added=models.DateField(_('Date Added'),auto_now_add=True)
    def __str__(self):
        return self.comment
    def get_absolute_url(self):
        return reverse('approve_detail',args=[str(self.id)])
    def save(self,*args,**kwargs):
        self.issue.status=1
        self.issue.save()
        super(Approverequest,self).save(*args,**kwargs)
    class Meta:
        verbose_name_plural=_('Approve Request')