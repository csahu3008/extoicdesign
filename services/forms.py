from django import forms
from .models import Approverequest,Raiserequest
from django.db.models import Q
class ApprovalForm(forms.ModelForm):
    class Meta:
        fields='__all__'
        model=Approverequest
    def __init__(self,*args,**kwargs):
        super(ApprovalForm,self).__init__(*args,**kwargs)
        self.fields['issue'].queryset=Raiserequest.objects.filter(Q(approverequest__isnull=True)|Q(approverequest=self.instance))