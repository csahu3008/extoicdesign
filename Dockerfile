FROM python:3.6
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONBUFFERED 1
RUN mkdir /dock
WORKDIR /dock
ADD . /dock/
RUN pip install pipenv 
COPY Pipfile Pipfile.lock /dock/
RUN pipenv install --system
COPY . /dock/