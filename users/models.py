from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import AbstractUser 
from django.contrib.auth.models import BaseUserManager
# from django.dispatch import receiver
# from django.db.models.signals import post_save
# Create your models here.
class ManageUser(BaseUserManager):
    def create_user(self,email,name,password=None,**extra_fields):
        if not email:
            raise ValueError("Email is Required ")
        email=self.normalize_email(email)
        extra_fields.setdefault('is_staff',True)
        if extra_fields.get('is_staff') is not True:
            raise ValueError("User must be staff")
        user=self.model(email=email,name=name,**extra_fields)
        user.set_password(password)
        user.save()
        return user
    def create_superuser(self,email,name,password,**extra_fields):
        extra_fields.setdefault('is_admin',True)
        extra_fields.setdefault('is_active',True)
        extra_fields.setdefault('is_superuser',True)
        if extra_fields.get('is_superuser') is not True:
            raise ValueError("User must be Superuser")
        if extra_fields.get('is_admin') is not True:
            raise ValueError("User must be admin")
        self.create_user(email,name,password,**extra_fields)
class MyUser(AbstractUser):
    username=None
    email=models.EmailField(_('Email'),unique=True,max_length=100)
    USERNAME_FIELD='email'
    name=models.CharField(_('Name'),max_length=200)
    password=models.CharField(_('Password'),max_length=255)
    mobile=models.CharField(_('Mobile Number'),max_length=100)
    is_admin=models.BooleanField(_('Admin'),default=False)
    EMAIL_FIELD='email'
    REQUIRED_FIELDS=['name','mobile']
    def __str__(self):
        return '{} is registered with {}'.format(self.name,self.email)
    @property
    def Admin(self):
        return self.is_admin
    objects=ManageUser()


class Profile(models.Model):
    user=models.OneToOneField(MyUser,on_delete=models.CASCADE)
    pic=models.ImageField(upload_to='profile/',default=None)
    date_joined=models.DateField(_('Date Joined'),auto_now_add=True,auto_now=False)
    date_of_birth=models.DateField(_('Date Of Birth'),auto_now=False,auto_now_add=False)
    def __str__(self):
        return self.user.email

# @receiver(post_save,sender=MyUser)
# def save_profile(sender,instance,**kwargs):
#     instance.profile.save()