from django.contrib import admin
from .models import MyUser,Profile
# from django.contrib.auth.forms import UserChangeForm,UserCreationForm
from django.contrib.auth.admin import UserAdmin
from .forms import ChangeForm,CreateForm
# Register your models here.
class ManageAdmin(UserAdmin):
    ordering=('email','name')
    list_display=['id','email','name','password']
    add_form=CreateForm
    add_fieldsets=(
        (None,{'classes':('wide',),'fields':('email','mobile','name','password','password2')}),
        )
    fieldsets=(
        (None,{'fields':('name','email','password','mobile')}),
        ('Designation',{'fields':('is_superuser','user_permissions')})
    )
    form=ChangeForm
admin.site.register(MyUser,ManageAdmin)
admin.site.register(Profile)