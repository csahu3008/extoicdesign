from django.contrib.auth.backends import ModelBackend
from django.conf import settings
from .models import MyUser
class MyBackend:
    def authenticate(self,email,password):
        try:
            if email and password is not None:
                user=MyUser.objects.get(email=email)
                if user is Not None:
                    if user.check_password(password):
                        return user
                    else:
                        return None
                else:
                    return None
            else:
                return user
        except MyUser.DoesNotExist:
            return None
    def get_user(self,user_id):
        try:
            user=MyUser.objects.get(pk=user_id)
            return user
        except MyUser.DoesNotExist:
            return None
