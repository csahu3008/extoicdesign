from django.contrib.auth.decorators import user_passes_test
from .models import MyUser
from django.contrib.auth import REDIRECT_FIELD_NAME
def admin_required(function=None,redirect_field_name=REDIRECT_FIELD_NAME,login_url='login'):
    actual_decorator=user_passes_test(lambda MyUser:MyUser.is_admin and MyUser.is_active,redirect_field_name=REDIRECT_FIELD_NAME,login_url='login')
    if function:
        return actual_decorator(function)
    return actual_decorator