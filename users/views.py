from django.shortcuts import render,redirect
from django.http import HttpResponse
from django.contrib.auth import authenticate,login
from .models import Profile
from django.views.generic import CreateView,FormView,DetailView
from django.contrib.messages import success
from django.core.mail import send_mail
from .forms import CreateForm,ProfileForm
class Signup(FormView):
    template_name='registration/signup.html'
    form_class=CreateForm
    second_form_class=ProfileForm
    success_url='login'

    def get_context_data(self,*args,**kwargs):
        context=super().get_context_data(*args,**kwargs)
        context['second']=self.second_form_class
        return context

    def post(self,request,*args,**kwargs):
        myform=self.form_class(request.POST)
        profileform=self.second_form_class(request.POST,request.FILES)
        if myform.is_valid():
            email=myform.cleaned_data['email']
            password=myform.cleaned_data['password']
            data=myform.save()
            if profileform.is_valid():
                    pro=profileform.save(commit=False)
                    pro.user=data
                    pro.save()
            user=authenticate(request,email=email,password=password)
            if user is not None:
                success(request,"Your account has been successfully created with email ID - {}".format(email))
                # send_mail('account created',"Your account has been successfully created with email ID - {}".format(email),'',[email,])
                login(request,user)
            return redirect('/')
        else:
            return HttpResponse('<h1>Invalid form</h1>')
class ProfileView(DetailView):
    model=Profile
    template_name='registration/profile.html'