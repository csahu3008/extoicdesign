from django import forms
from django.utils.translation import ugettext as _
from users.models import MyUser,Profile
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from django.core.exceptions import ValidationError
from django.contrib.auth.forms import AuthenticationForm
class CreateForm(forms.ModelForm):
    class Meta:
        model=MyUser
        fields=('name','email','mobile','password')
    name=forms.CharField(max_length=50,widget=forms.TextInput(attrs={'id':'name'}))
    email=forms.EmailField(max_length=100,widget=forms.TextInput(attrs={'id':'email'}))
    password=forms.CharField(max_length=40,widget=forms.PasswordInput(attrs={'id':'password'}))
    password2=forms.CharField(max_length=40,widget=forms.PasswordInput(attrs={'id':'password2'}))
    mobile=forms.CharField(max_length=10,widget=forms.TextInput(attrs={'id':'mobile'}))
    def clean_password2(self):
        p1=self.cleaned_data['password']
        p2=self.cleaned_data['password2']
        if p1 != p2:
            raise forms.ValidationError(_("Sorry Passwords don not match"))
    def save(self,commit=True):
        user=super().save(commit=False)
        user.set_password(self.cleaned_data.get('password'))       
        if commit:
            user.save()
        return user
class ChangeForm(forms.ModelForm):
    class Meta:
        model=MyUser
        fields='__all__'
    password1=ReadOnlyPasswordHashField()
    def clean_password(self):
        return self.initial['password']

class ProfileForm(forms.ModelForm):
    class Meta:
        model=Profile
        fields=('pic','date_of_birth')
    pic=forms.ImageField(widget=forms.FileInput(attrs={'class':'photo','type':'file'}))
    date_of_birth=forms.DateField(widget=forms.DateInput(attrs={'class':'dob','type':'date'}))

