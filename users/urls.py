from django.contrib import admin
from django.urls import path,include
from .views import Signup,ProfileView
urlpatterns=[
    path('signup/',Signup.as_view(),name='signup')      
,path('profile/<int:pk>/',ProfileView.as_view(),name='profile')
]